setwd("C:/Users/amitg/Desktop/Rstudio/data base")
weather.raw <- read.csv("weatherAUS.csv")

str(weather.raw)
summary(weather.raw)

weather.prepard <- weather.raw

#���� ��� ��� �������� ��� ������ ����� ����� (na)
isThereNa <- function(df){
  column <- apply(df, 2, function(x) any(is.na(x)))
  return(column)
}

isThereNa(weather.prepard)
#���� ��� �� ��� �� ������ �� ������ ����� ��� ���
#����� �� ������� �� �� �� ������� ������� ����� �������
medi <- median(weather.prepard$MinTemp, na.rm = TRUE)
weather.prepard$MinTemp[is.na(weather.prepard$MinTemp)] <- medi

medi <- median(weather.prepard$MaxTemp, na.rm = TRUE)
weather.prepard$MaxTemp[is.na(weather.prepard$MaxTemp)] <- medi

medi <- median(weather.prepard$Rainfall, na.rm = TRUE)
weather.prepard$Rainfall[is.na(weather.prepard$Rainfall)] <- medi

medi <- median(weather.prepard$MinTemp, na.rm = TRUE)
weather.prepard$MinTemp[is.na(weather.prepard$MinTemp)] <- medi

medi <- median(weather.prepard$Evaporation, na.rm = TRUE)
weather.prepard$Evaporation[is.na(weather.prepard$Evaporation)] <- medi

medi <- median(weather.prepard$Sunshine, na.rm = TRUE)
weather.prepard$Sunshine[is.na(weather.prepard$Sunshine)] <- medi

medi <- median(weather.prepard$WindGustSpeed, na.rm = TRUE)
weather.prepard$WindGustSpeed[is.na(weather.prepard$WindGustSpeed)] <- medi

medi <- median(weather.prepard$WindSpeed9am, na.rm = TRUE)
weather.prepard$WindSpeed9am[is.na(weather.prepard$WindSpeed9am)] <- medi

medi <- median(weather.prepard$WindSpeed3pm, na.rm = TRUE)
weather.prepard$WindSpeed3pm[is.na(weather.prepard$WindSpeed3pm)] <- medi

medi <- median(weather.prepard$Humidity9am, na.rm = TRUE)
weather.prepard$Humidity9am[is.na(weather.prepard$Humidity9am)] <- medi

medi <- median(weather.prepard$Humidity3pm, na.rm = TRUE)
weather.prepard$Humidity3pm[is.na(weather.prepard$Humidity3pm)] <- medi

medi <- median(weather.prepard$Pressure9am, na.rm = TRUE)
weather.prepard$Pressure9am[is.na(weather.prepard$Pressure9am)] <- medi

medi <- median(weather.prepard$Pressure3pm, na.rm = TRUE)
weather.prepard$Pressure3pm[is.na(weather.prepard$Pressure3pm)] <- medi

medi <- median(weather.prepard$Cloud9am, na.rm = TRUE)
weather.prepard$Cloud9am[is.na(weather.prepard$Cloud9am)] <- medi

medi <- median(weather.prepard$Cloud3pm, na.rm = TRUE)
weather.prepard$Cloud3pm[is.na(weather.prepard$Cloud3pm)] <- medi

medi <- median(weather.prepard$Temp9am, na.rm = TRUE)
weather.prepard$Temp9am[is.na(weather.prepard$Temp9am)] <- medi

medi <- median(weather.prepard$Temp3pm, na.rm = TRUE)
weather.prepard$Temp3pm[is.na(weather.prepard$Temp3pm)] <- medi

isThereNa(weather.prepard)

#���� �� �� ������ ��� ����� �� ��� 
weather.prepard <- na.omit(weather.prepard)

isThereNa(weather.prepard)

library(caTools)

filter <- sample.split(weather.prepard$RainTomorrow, SplitRatio = 0.7)

weather.train <- subset(weather.prepard, filter==T)
weather.test <- subset(weather.prepard, filter==F)

dim(weather.train)
dim(weather.test)

weather.model <- glm(RainTomorrow~., family=binomial(link = 'logit'), data = weather.train)

summary(weather.model)

predicted.weather.test <- predict(weather.model, newdata = weather.test, type = 'response')
predicted.weather.test

confusion.matrix <- table(predicted.weather.test> 0.5, weather.test$RainTomorrow)
confusion.matrix

precision <- confusion.matrix[2,2]/(confusion.matrix[2,2]+confusion.matrix[2,1])
precision

recall <- confusion.matrix[2,2]/(confusion.matrix[2,2]+confusion.matrix[1,2])
recall


